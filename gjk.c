#include "gjk.h"

#include <stdio.h>

static v3 support_minkowski(object o1, object o2, v3 dir) {
	v3 a = support(o1, dir);
	v3 b = v3_negate(support(o2, v3_negate(dir)));
	return v3_add(a, b);
}

static int simplex2(v3 *points, int *count, v3 *Dir) {
	v3 A = points[1];
	v3 B = points[0];

	v3 AB = v3_sub(B, A);
	v3 AO = v3_negate(A);
	v3 side = v3_cross(AB, AO);
	*Dir = v3_normalize(v3_cross(side, AB));
	return 0;
}

static int simplex3(v3 *points, int *count, v3 *Dir) {
	v3 A = points[2];
	v3 B = points[1];
	v3 C = points[0];

	v3 AC = v3_sub(C, A);
	v3 AB = v3_sub(B, A);
	v3 up = v3_cross(AC, AB);
	v3 ABout = v3_cross(up, AB);
	v3 AO = v3_negate(A);

	if (v3_dot(AO, ABout) > 0) {
		points[0] = A;
		v3 side = v3_cross(AB, AO);
		*Dir = v3_normalize(v3_cross(side, AB));
		*count = 2;
		return 0;
	}

	v3 ACout = v3_cross(AC, up);

	if (v3_dot(AO, ACout) > 0) {
		points[1] = A;
		v3 side = v3_cross(AC, AO);
		*Dir = v3_normalize(v3_cross(side, AC));
		*count = 2;
		return 0;
	}

	up = v3_normalize(up);

	if (v3_dot(AO, up) > 0) {
		*Dir = up;
		return 0;
	}

	points[0] = B;
	points[1] = C;
	*Dir = v3_negate(up);
	return 0;
}

static int simplex4(v3 *points, int *count, v3 *Dir) {
	v3 A = points[3];
	v3 B = points[2];
	v3 C = points[1];
	v3 D = points[0];

	v3 AO = v3_negate(A);

	v3 AB = v3_sub(B, A);
	v3 AC = v3_sub(C, A);
	v3 AD = v3_sub(D, A);
	v3 t1out = v3_cross(AC, AB);
	v3 t2out = v3_cross(AB, AD);
	v3 t3out = v3_cross(AD, AC);

	v3 ABt1 = v3_cross(t1out, AB);
	v3 ABt2 = v3_cross(AB, t2out);
	v3 ACt1 = v3_cross(AC, t1out);
	v3 ACt3 = v3_cross(t3out, AC);
	v3 ADt2 = v3_cross(t2out, AD);
	v3 ADt3 = v3_cross(AD, t3out);

	int AB1 = v3_dot(ABt1, AO) > 0;
	int AB2 = v3_dot(ABt2, AO) > 0;
	int AC1 = v3_dot(ACt1, AO) > 0;
	int AC3 = v3_dot(ACt3, AO) > 0;
	int AD2 = v3_dot(ADt2, AO) > 0;
	int AD3 = v3_dot(ADt3, AO) > 0;

	int t1 = v3_dot(t1out, AO) > 0;
	int t2 = v3_dot(t2out, AO) > 0;
	int t3 = v3_dot(t3out, AO) > 0;

	if (AB1 && AB2) {
		points[0] = B;
		points[1] = A;
		*count = 2;
		v3 side = v3_cross(AB, AO);
		*Dir = v3_normalize(v3_cross(side, AB));
		return 0;
	}

	if (AC1 && AC3) {
		points[0] = A;
		*count = 2;
		v3 side = v3_cross(AC, AO);
		*Dir = v3_normalize(v3_cross(side, AC));
		return 0;
	}

	if (AD2 && AD3) {
		points[1] = A;
		*count = 2;
		v3 side = v3_cross(AD, AO);
		*Dir = v3_normalize(v3_cross(side, AD));
		return 0;
	}

	if (t1 && !AB1 && !AC1) {
		points[0] = A;
		*count = 3;
		*Dir = v3_normalize(t1out);
		return 0;
	}

	if (t2 && !AB2 && !AD2) {
		points[1] = A;
		*count = 3;
		*Dir = v3_normalize(t2out);
		return 0;
	}

	if (t3 && !AC3 && !AD3) {
		points[2] = A;
		*count = 3;
		*Dir = v3_normalize(t3out);
		return 0;
	}

	return 1;
}

static int simplex(v3 *points, int *count, v3 *D) {
	switch (*count) {
	case 2:
		return simplex2(points, count, D);
	case 3:
		return simplex3(points, count, D);
	case 4:
		return simplex4(points, count, D);
	}
	return 0;
}

int gjk(object o1, object o2, int *its) {
	v3 points[4];
	int count = 1;
	v3 D = {0, 0, 1};
	v3 A = support_minkowski(o1, o2, D);
	points[0] = A;
	D = v3_negate(A);
	D = v3_normalize(D);
	*its = 1;

	for(;;) {
		if (fabsf(v3_dot(points[0], D)) < 0.00001f) {
			return 1;
		}
		(*its)++;
		A = support_minkowski(o1, o2, D);
		if (v3_dot(A, D) < 0) {
			return 0;
		}
		points[count] = A;
		count++;
		if (simplex(points, &count, &D)) {
			return 1;
		}
	}
}

static int simplexd2(v3 *points, int *count, v3 *Dir) {
	v3 A = points[1];
	v3 B = points[0];
	v3 AB = v3_sub(B, A);

	if (v3_dot(A, AB) > 0) {
		points[0] = A;
		*count = 1;
		*Dir = v3_normalize(v3_negate(A));
		return 0;
	}

	v3 AO = v3_negate(A);
	v3 side = v3_cross(AB, AO);
	*Dir = v3_normalize(v3_cross(side, AB));
	return 0;
}


static int simplexd3(v3 *points, int *count, v3 *Dir) {
	v3 A = points[2];
	v3 B = points[1];
	v3 C = points[0];

	v3 AC = v3_sub(C, A);
	v3 AB = v3_sub(B, A);
	v3 up = v3_cross(AC, AB);
	v3 ABout = v3_cross(up, AB);
	v3 AO = v3_negate(A);

	if (v3_dot(AO, ABout) > 0) {
		points[0] = A;
		if (v3_dot(AO, AB) < 0) {
			if (v3_dot(AO, AC) < 0) {
				*count = 1;
				*Dir = v3_normalize(v3_negate(A));
				return 0;
			} else {
				points[1] = C;
				v3 side = v3_cross(AC, AO);
				*Dir = v3_normalize(v3_cross(side, AC));
				*count = 2;
				return 0;
			}
		}
		v3 side = v3_cross(AB, AO);
		*Dir = v3_normalize(v3_cross(side, AB));
		*count = 2;
		return 0;
	}

	v3 ACout = v3_cross(AC, up);

	if (v3_dot(AO, ACout) > 0) {
		if (v3_dot(AO, AC) < 0) {
			points[0] = A;
			*count = 1;
			*Dir = v3_normalize(v3_negate(A));
			return 0;
		}
		points[1] = A;
		v3 side = v3_cross(AC, AO);
		*Dir = v3_normalize(v3_cross(side, AC));
		*count = 2;
		return 0;
	}

	up = v3_normalize(up);

	if (v3_dot(AO, up) > 0) {
		*Dir = up;
		return 0;
	}

	points[0] = B;
	points[1] = C;
	*Dir = v3_negate(up);
	return 0;
}

static int simplexd4(v3 *points, int *count, v3 *Dir) {
	v3 A = points[3];
	v3 B = points[2];
	v3 C = points[1];
	v3 D = points[0];

	v3 AO = v3_negate(A);

	v3 AB = v3_sub(B, A);
	v3 AC = v3_sub(C, A);
	v3 AD = v3_sub(D, A);
	v3 t1out = v3_cross(AC, AB);
	v3 t2out = v3_cross(AB, AD);
	v3 t3out = v3_cross(AD, AC);

	v3 ABt1 = v3_cross(t1out, AB);
	v3 ABt2 = v3_cross(AB, t2out);
	v3 ACt1 = v3_cross(AC, t1out);
	v3 ACt3 = v3_cross(t3out, AC);
	v3 ADt2 = v3_cross(t2out, AD);
	v3 ADt3 = v3_cross(AD, t3out);

	int AB1 = v3_dot(ABt1, AO) > 0;
	int AB2 = v3_dot(ABt2, AO) > 0;
	int AC1 = v3_dot(ACt1, AO) > 0;
	int AC3 = v3_dot(ACt3, AO) > 0;
	int AD2 = v3_dot(ADt2, AO) > 0;
	int AD3 = v3_dot(ADt3, AO) > 0;

	int t1 = v3_dot(t1out, AO) > 0;
	int t2 = v3_dot(t2out, AO) > 0;
	int t3 = v3_dot(t3out, AO) > 0;

	int ABO = v3_dot(AB, AO) > 0;
	int ACO = v3_dot(AD, AO) > 0;
	int ADO = v3_dot(AD, AO) > 0;

	if (AB1 && AB2 && ABO) {
		points[0] = B;
		points[1] = A;
		*count = 2;
		v3 side = v3_cross(AB, AO);
		*Dir = v3_normalize(v3_cross(side, AB));
		return 0;
	}

	if (AC1 && AC3 && ACO) {
		points[0] = A;
		*count = 2;
		v3 side = v3_cross(AC, AO);
		*Dir = v3_normalize(v3_cross(side, AC));
		return 0;
	}

	if (AD2 && AD3 && ADO) {
		points[1] = A;
		*count = 2;
		v3 side = v3_cross(AD, AO);
		*Dir = v3_normalize(v3_cross(side, AD));
		return 0;
	}

	if (t1 && !AB1 && !AC1) {
		points[0] = A;
		*count = 3;
		*Dir = v3_normalize(t1out);
		return 0;
	}

	if (t2 && !AB2 && !AD2) {
		points[1] = A;
		*count = 3;
		*Dir = v3_normalize(t2out);
		return 0;
	}

	if (t3 && !AC3 && !AD3) {
		points[2] = A;
		*count = 3;
		*Dir = v3_normalize(t3out);
		return 0;
	}

	if (t1 && t2 && t3) {
		points[0] = A;
		*count = 1;
		*Dir = v3_normalize(AO);
		return 0;
	}

	return 1;
}

static int simplexd(v3 *points, int *count, v3 *D) {
	switch (*count) {
	case 2:
		return simplexd2(points, count, D);
	case 3:
		return simplexd3(points, count, D);
	case 4:
		return simplexd4(points, count, D);
	}
	return 0;
}

float gjkd(object o1, object o2, int *its) {
	v3 points[4];
	int count = 1;
	v3 D = {0, 0, 1};
	v3 A = support_minkowski(o1, o2, D);
	points[0] = A;
	D = v3_negate(A);
	D = v3_normalize(D);
	*its = 1;

	for (;;) {
		(*its)++;
		A = support_minkowski(o1, o2, D);
		v3 rel = v3_sub(A, points[0]);
		if (v3_dot(rel, D) < 0.00001f) {
			return -v3_dot(points[0], D);
		}
		points[count] = A;
		count++;
		
		if (simplexd(points, &count, &D)) {
			return 0.f;
		}
	}
}
