CFLAGS = --std=c99
LDFLAGS = -lm

all: gjk gjkd

gjk: main.c object.c v3.c gjk.c object.h v3.h gjk.h
	$(CC) $(CFLAGS) -DDISTANCE=0 main.c object.c v3.c gjk.c -o gjk $(LDFLAGS)

gjkd: main.c object.c v3.c gjk.c object.h v3.h gjk.h
	$(CC) $(CFLAGS) -DDISTANCE=1 main.c object.c v3.c gjk.c -o gjkd $(LDFLAGS)

.PHONY: clean test all

clean:
	rm -f gjk gjkd

test: gjk gjkd
	./gjk
	./gjkd
