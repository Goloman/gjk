#include "gjk.h"

#include <stdio.h>

#define ARR_SIZE(x) sizeof(x) / sizeof(x[0])

#ifndef DISTANCE
#define DISTANCE 0
#endif

int verbose = 1;

int test_number = 0;

int failed_count = 0;

v3 h1p[] = {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

void test(object o1, object o2, int expected) {
	int iter_count = 0;
#if DISTANCE
	float res = gjkd(o1, o2, &iter_count);
	int result = res == 0;
#else
	int result = gjk(o1, o2, &iter_count);
#endif
	int passed = result == expected;

	printf("TEST % 3d - %s (%d)\n", test_number++, passed ? "PASSED" : "FAILED", iter_count);

	if (!passed) failed_count++;

	if (verbose && !passed) {
		print(o1);
		print(o2);
		if (result) {
			printf("Unexpected intersection found\n");
		} else {
			printf("Expected intersection not found\n");
		}
	}
}

int main(int argc, char **argv) {
	object b1 = make_ball(1, V3(1, 1, 0));
	object b2 = make_ball(1, V3(0, 0, 0));
	object b3 = make_ball(1, V3(1, 0, 0));
	object b4 = make_ball(1, V3(0, 1, 0));
	object b5 = make_ball(1, V3(0, 0, 1));
	object b6 = make_ball(1, V3(0, 0, 0));
	object b7 = make_ball(1, V3(1, 2, 2));
	object a1 = make_aabb(V3(1, 1, 1), V3(0, 0, 0));
	object a2 = make_aabb(V3(0.3, 1.2, 1), V3(-1.2, 2.1, 1));
	object h1 = make_hull(h1p, ARR_SIZE(h1p));

	test(a1, b1, 1);
	test(a1, b2, 1);
	test(a1, b3, 1);
	test(a1, b4, 1);
	test(a1, b5, 1);
	test(a1, a2, 1);
	test(a2, b1, 0);
	test(b6, b7, 0);
	test(h1, b1, 1);
	test(h1, b7, 0);

	if (failed_count) {
		printf("\n%d tests failed\n\n", failed_count);
	} else {
		printf("\nall tests passed\n\n");
	}

	return 0;
}

