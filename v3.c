#include "v3.h"

#include <math.h>

v3 v3_add(v3 lhs, v3 rhs) {
	v3 ret = {
		lhs.x + rhs.x,
		lhs.y + rhs.y,
		lhs.z + rhs.z,
	};
	return ret;
}

v3 v3_sub(v3 lhs, v3 rhs) {
	v3 ret = {
		lhs.x - rhs.x,
		lhs.y - rhs.y,
		lhs.z - rhs.z,
	};
	return ret;
}

v3 v3_mul(v3 lhs, float rhs) {
	v3 ret = {
		lhs.x * rhs,
		lhs.y * rhs,
		lhs.z * rhs,
	};
	return ret;
}

v3 v3_div(v3 lhs, float rhs) {
	v3 ret = {
		lhs.x / rhs,
		lhs.y / rhs,
		lhs.z / rhs,
	};
	return ret;
}

float v3_dot(v3 lhs, v3 rhs) {
	return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}

v3 v3_cross(v3 lhs, v3 rhs) {
	v3 ret = {
		lhs.y * rhs.z - lhs.z * rhs.y,
		lhs.z * rhs.x - lhs.x * rhs.z,
		lhs.x * rhs.y - lhs.y * rhs.x,
	};
	return ret;
}

v3 v3_negate(v3 arg) {
	return v3_mul(arg, -1.f);
}

float v3_length(v3 arg) {
	return sqrtf(v3_dot(arg, arg));
}

v3 v3_normalize(v3 arg) {
	return v3_div(arg, v3_length(arg));
}

v3 V3(float x, float y, float z) {
	v3 ret = {x, y, z};
	return ret;
}
