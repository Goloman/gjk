#ifndef OBJECT_H
#define OBJECT_H

#include "v3.h"

#include <stddef.h>

typedef enum object_type {
	OBJECT_BALL,
	OBJECT_AABB,
	OBJECT_HULL,
} object_type;

typedef struct ball {
	v3 point;
	float radius;
} ball;

typedef struct aabb {
	v3 point;
	v3 radius;
} aabb;

typedef struct hull {
	v3 *points;
	size_t count;
} hull;

typedef struct object {
	object_type type;
	union {
		ball ball;
		aabb aabb;
		hull hull;
	};
} object;

v3 support(object o, v3 d);
void print(object o);

object make_ball(float radius, v3 point);
object make_aabb(v3 radii, v3 point);
object make_hull(v3 *points, size_t count);

#endif //OBJECT_H
