#include "object.h"

#include <stdio.h>

static float sign(float x) {
	if (x > 0) return 1;
	if (x < 0) return -1;
	return 0;
}

static v3 ball_support(ball b, v3 d) {
	v3 ret = v3_mul(d, b.radius);
	return v3_add(b.point, ret);
}

static v3 aabb_support(aabb b, v3 d) {
	v3 ret = {
		sign(d.x) * b.radius.x + b.point.x,
		sign(d.y) * b.radius.y + b.point.y,
		sign(d.z) * b.radius.z + b.point.z,
	};
	return ret;
}

static v3 hull_support(hull h, v3 d) {
	v3 ret = {0, 0, 0};
	if (!h.count) return ret;
	ret = h.points[0];
	float a = v3_dot(ret, d);
	for (size_t i = 1; i < h.count; i++) {
		v3 candidate = h.points[i];
		float ca = v3_dot(candidate, d);
		if (ca < a) continue;
		ret = candidate;
		a = ca;
	}
	return ret;
}

v3 support(object o, v3 d) {
	switch (o.type) {
	case OBJECT_BALL:
		return ball_support(o.ball, d);
	case OBJECT_AABB:
		return aabb_support(o.aabb, d);
	case OBJECT_HULL:
		return hull_support(o.hull, d);
	}
}

void ball_print(ball b) {
	printf("BALL - R: %f P: [%f, %f, %f]\n", b.radius, b.point.x, b.point.y, b.point.z);
}

void aabb_print(aabb b) {
	printf("AABB - R: [%f, %f, %f] P: [%f, %f, %f]\n", b.radius.x, b.radius.y, b.radius.z, b.point.x, b.point.y, b.point.z);
}

void hull_print(hull h) {
	printf("HULL - ");
	for (size_t i = 0; i < h.count; i++) {
		printf("[%f, %f, %f], ", h.points[i].x, h.points[i].y, h.points[i].z);
	}
	printf("\n");
}

void print(object o) {
	switch (o.type) {
	case OBJECT_BALL:
		return ball_print(o.ball);
	case OBJECT_AABB:
		return aabb_print(o.aabb);
	case OBJECT_HULL:
		return hull_print(o.hull);
	}
}

object make_ball(float radius, v3 point) {
	object ret;
	ret.type = OBJECT_BALL;
	ret.ball.point = point;
	ret.ball.radius = radius;
	return ret;
}

object make_aabb(v3 radii, v3 point) {
	object ret;
	ret.type = OBJECT_AABB;
	ret.aabb.point = point;
	ret.aabb.radius = radii;
	return ret;
}

object make_hull(v3 *points, size_t count) {
	object ret;
	ret.type = OBJECT_HULL;
	ret.hull.points = points;
	ret.hull.count = count;
	return ret;
}
