#ifndef V3_H
#define V3_H

typedef struct v3 {
	float x, y, z;
} v3;

v3 v3_add(v3 lhs, v3 rhs);
v3 v3_sub(v3 lhs, v3 rhs);
v3 v3_mul(v3 lhs, float rhs);
v3 v3_div(v3 lhs, float rhs);
float v3_dot(v3 lhs, v3 rhs);
v3 v3_cross(v3 lhs, v3 rhs);
v3 v3_negate(v3 arg);
float v3_length(v3 arg);
v3 v3_normalize(v3 arg);
v3 V3(float x, float y, float z);

#endif //V3_H
